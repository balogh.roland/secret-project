module.exports = (err, req, res, next) => {
  console.error(err);
  if (err.code && !isNaN(err.code)) return res.status(err.code).json({ data: null, error: err.message });
  res.status(500).json({ data: null, error: 'Internal Server Error' });
}