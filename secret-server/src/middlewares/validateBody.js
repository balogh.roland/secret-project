const Joi = require('@hapi/joi');

const secretSchema = Joi.object({
  secret: Joi.string().required(),
  expireAfterViews: Joi.number().greater(0).required(),
  expireAfter: Joi.number().min(0).default(0)
});

module.exports = (req, res, next) => {
  const result = secretSchema.validate(req.body);
  if (result.error) {
    return res.status(400).json({ data: null, error: result.error.details.map(err => err.message).join(',') });
  }
  next();
};
