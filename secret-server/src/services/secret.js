const { ObjectId }  = require('mongodb');
const { getDatabase } = require('../utils/database');
const { getExpiresAt, getIsExpired } = require('../utils/date');
const { encrypt, decrypt } = require('../utils/encryption');

module.exports = {
  createSecret: async ({ secret, expireAfter, expireAfterViews }) => {
    const database = getDatabase();
    const _id = ObjectId();
    const hash = encrypt(secret, _id);

    await database.collection('secrets').insertOne({
      _id,
      hash,
      remainingViews: expireAfterViews,
      expiresAt: getExpiresAt(_id.getTimestamp(), expireAfter)
    });

    return database.collection('secrets').findOne({ hash });
  },
  getSecret: async hash => {
    const database = getDatabase();
    const secret = await database.collection('secrets').findOne({ hash });

    if (!secret) throw { code: 404, message: 'Not Found' };

    return secret;
  },
  validateSecret: async ({ _id, expiresAt, remainingViews, hash }) => {
    const database = getDatabase();
    const isExpired = getIsExpired(_id.getTimestamp(), expiresAt);

    if (isExpired || !remainingViews) {
      await database.collection('secrets').deleteOne({ hash });
      throw { code: 404, message: 'Not Found' };
    }

    const secretText = decrypt(hash, _id);

    return secretText;
  },
  updateSecret: async (hash, update) => {
    const database = getDatabase();
    const result = await database.collection('secrets').findOneAndUpdate({ hash }, update, { returnOriginal: false });

    return result.value;
  },
  transformSecret: ({ _id, hash, secretText, expiresAt, remainingViews }) => ({
    hash,
    secretText,
    createdAt: _id.getTimestamp(),
    expiresAt,
    remainingViews
  })
};
