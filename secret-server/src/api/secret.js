require('express-async-errors');
const express = require('express');
const validateBody = require('../middlewares/validateBody');
const errorHandler = require('../middlewares/errorHandler');
const { createSecret, getSecret, validateSecret, updateSecret, transformSecret } = require('../services/secret');

const secretApi = express.Router();

 /**
   * @api {get} /secret/:hash Get secret
   * @apiName Get secret
   * @apiGroup Secret
   *
   * @apiParam {String} hash The hash you can unlock the secret with
   *
   * @apiSuccess {Object} response Response object
   * @apiSuccess {String} response.error Error (should be null when success)
   * @apiSuccess {Object} response.data Payload
   * @apiSuccess {String} response.data.hash The hash of the string
   * @apiSuccess {String} response.data.secretText The original text
   * @apiSuccess {String} response.data.createdAt Date of creation
   * @apiSuccess {String} response.data.expiresAt Date of expiration
   * @apiSuccess {Number} response.data.remainingViews Number of remaining views
   * 
   * @apiError {Object} response Response object
   * @apiError {String} response.error Error
   * @apiError {String} response.data Payload (should be null when error)
   */
secretApi.get('/secret/:hash', async (req, res) => {
  const { hash } = req.params;
  let secret = await getSecret(hash);
  const secretText = await validateSecret(secret);
  secret = await updateSecret(hash, { $inc: { remainingViews: -1 } });
  res.status(200).json({ data: transformSecret({ ...secret, secretText }), error: null });
});

 /**
   * @api {post} /secret Create hash
   * @apiName Create hash
   * @apiGroup Secret
   *
   * @apiParam {String} secret This text that will be saved as a secret
   * @apiParam {Number} expireAfterViews The secret won't be available after the given number of views
   * @apiParam {Number} expireAfter The secret won't be available after the given time. The value is provided in minutes. 0 means never expires
   *
   * @apiSuccess {Object} response Response object
   * @apiSuccess {String} response.error Error (should be null when success)
   * @apiSuccess {Object} response.data Payload
   * @apiSuccess {String} response.data.hash The hash of the string
   * @apiSuccess {String} response.data.secretText The original text
   * @apiSuccess {String} response.data.createdAt Date of creation
   * @apiSuccess {String} response.data.expiresAt Date of expiration
   * @apiSuccess {Number} response.data.remainingViews Number of remaining views
   * 
   * @apiError {Object} response Response object
   * @apiError {String} response.error Error
   * @apiError {String} response.data Payload (should be null when error)
   */
secretApi.post('/secret', validateBody, async (req, res) => {
  const secret = await createSecret(req.body);
  res.status(200).json({ data: transformSecret({ ...secret, secretText: req.body.secret }), error: null });
});

secretApi.use(errorHandler);

module.exports = secretApi;
