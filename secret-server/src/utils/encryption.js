const crypto = require('crypto');
const resizedInitVector = Buffer.alloc(16, 0);
const initVector = crypto.createHash('sha256').update(process.env.HASH_SECRET || 'defaultsecret').digest();

initVector.copy(resizedInitVector);

module.exports = {
  encrypt: (secret, keySecret) => {
    const key = crypto.createHash('sha256').update(keySecret.toString()).digest();
    const cipher = crypto.createCipheriv('aes256', key, resizedInitVector);
    const hash = cipher.update(secret, 'binary', 'hex');
    return hash + cipher.final('hex');
  },
  decrypt: (hash, keySecret) => {
    const key = crypto.createHash('sha256').update(keySecret.toString()).digest();
    const decipher = crypto.createDecipheriv('aes256', key, resizedInitVector);
    const secret = decipher.update(hash, 'hex', 'binary');
    return secret + decipher.final('binary');
  }
};
