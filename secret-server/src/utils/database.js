const { MongoClient } = require('mongodb');
const dbName = process.env.DB_NAME || 'secret-db';
const url = process.env.DB_URL || 'mongodb://localhost:27017';
let database;

module.exports = {
  initDatabase: async () => {
    const client = await MongoClient.connect(url, { useUnifiedTopology: true, serverSelectionTimeoutMS: 1000 });
    database = client.db(dbName);
    database.collection('secrets');
    Promise.resolve();
  },
  getDatabase: () => database,
};
