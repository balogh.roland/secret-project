module.exports = {
  getExpiresAt: (createdAt, expireAfter) => new Date(createdAt.getTime() + expireAfter * 60000),
  getIsExpired: (createdAt, expiresAt) =>
    createdAt.getTime() !== expiresAt.getTime() && expiresAt.getTime() < new Date().getTime()
};
