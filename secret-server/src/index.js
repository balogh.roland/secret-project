require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const { initDatabase } = require('./utils/database');
const secretApi = require('./api/secret');
const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(bodyparser.json());

app.use('/api', secretApi);

initDatabase()
  .then(() => app.listen(port, () => console.log(`Server is listening at port: ${port}`)))
  .catch(error => {
    console.log('Database connection error: ', error);
    process.exit(1);
  });

module.exports = app;
