const supertest = require('supertest');
const app = require('../src/index');
const { initDatabase, getDatabase } = require('../src/utils/database');

const request = supertest(app);

global.Request = request;
global.expect = require('chai').expect;

before(async () => {
  await initDatabase().catch(error => {
    console.log(`Database connection error: ${error}`);
    process.exit(1);
  });
  global.database = getDatabase();
  return app;
});

after(() => database.dropDatabase());
