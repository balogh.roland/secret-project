const { createSecret } = require('../../src/services/secret');
const secretText = 'My secret';

describe('POST /api/secret', () => {
  it('should return error for secret', async () => {
    const res = await Request.post('/api/secret')
      .send({ secret: 12, expireAfterViews: 3, expireAfter: 0 })
      .set('Content-Type', 'application/json');

    expect(res.status).to.equal(400);
    expect(res.body.data).to.equal(null);
    expect(res.body.error).to.be.a('string');
  });

  it('should return error for expireAfterViews', async () => {
    const res = await Request.post('/api/secret')
      .send({ secret: secretText, expireAfterViews: -1, expireAfter: 0 })
      .set('Content-Type', 'application/json');

    expect(res.status).to.equal(400);
    expect(res.body.data).to.equal(null);
    expect(res.body.error).to.be.a('string');
  });

  it('should return error for expireAfter', async () => {
    const res = await Request.post('/api/secret')
      .send({ secret: secretText, expireAfterViews: 11, expireAfter: -1 })
      .set('Content-Type', 'application/json');

    expect(res.status).to.equal(400);
    expect(res.body.data).to.equal(null);
    expect(res.body.error).to.be.a('string');
  });

  it('should create a secret', async () => {
    const res = await Request.post('/api/secret')
      .send({ secret: secretText, expireAfterViews: 11, expireAfter: 0 })
      .set('Content-Type', 'application/json');

    const savedSecret = await database.collection('secrets').findOne({ hash: res.body.data.hash });

    expect(res.status).to.equal(200);
    expect(res.body.error).to.equal(null);
    expect(res.body.data).to.have.all.keys('hash', 'secretText', 'createdAt', 'expiresAt', 'remainingViews');
    expect(res.body.data.remainingViews).to.equal(savedSecret.remainingViews);
    expect(res.body.data.createdAt).to.equal(res.body.data.expiresAt);
    expect(res.body.data.secretText).to.equal(secretText);
    expect(res.body.data.hash).to.equal(savedSecret.hash);
    expect(savedSecret).to.be.a('object');
  });
});

describe('GET /api/secret/:hash', () => {
  it('should return a not found error', async () => {
    const res = await Request.get(`/api/secret/asd`).set('Content-Type', 'application/json');

    expect(res.status).to.equal(404);
    expect(res.body.data).to.equal(null);
    expect(res.body.error).to.be.a('string');
  });

  it('should return a not found error, and delete invalid secret (no remaining views)', async () => {
    const savedSecret = await createSecret({ secret: secretText, expireAfter: 0, expireAfterViews: 0 });
    const res = await Request.get(`/api/secret/${savedSecret.hash}`).set('Content-Type', 'application/json');

    const secretFromDb = await database.collection('secret').findOne({ hash: savedSecret.hash });

    expect(res.status).to.equal(404);
    expect(res.body.data).to.equal(null);
    expect(res.body.error).to.be.a('string');
    expect(secretFromDb).to.equal(null);
  });

  it('should return a not found error, and delete invalid secret (expired)', async () => {
    const savedSecret = await createSecret({ secret: secretText, expireAfter: 0, expireAfterViews: 100 });
    const expiresAt = new Date(2020, 3, 24, 10, 33);
    await database
      .collection('secrets')
      .updateOne({ hash: savedSecret.hash }, { $set: { expiresAt } });
    const res = await Request.get(`/api/secret/${savedSecret.hash}`).set('Content-Type', 'application/json');

    const secretFromDb = await database.collection('secret').findOne({ hash: savedSecret.hash });

    expect(res.status).to.equal(404);
    expect(res.body.data).to.equal(null);
    expect(res.body.error).to.be.a('string');
    expect(secretFromDb).to.equal(null);
  });

  it('should return the secret, decrease view count', async () => {
    const savedSecret = await createSecret({ secret: secretText, expireAfter: 0, expireAfterViews: 100 });
    const res = await Request.get(`/api/secret/${savedSecret.hash}`).set('Content-Type', 'application/json');

    expect(res.status).to.equal(200);
    expect(res.body.error).to.equal(null);
    expect(res.body.data).to.have.all.keys('hash', 'secretText', 'createdAt', 'expiresAt', 'remainingViews');
    expect(res.body.data.hash).to.equal(savedSecret.hash);
    expect(res.body.data.remainingViews).to.equal(99);
    expect(res.body.data.secretText).to.equal(secretText);
  });
});
