import Vue from 'vue';
import Vuex from 'vuex';
import moment from 'moment';
import request from '../utils/request';

Vue.use(Vuex);

const getDataWithDate = data => {
  return {
    ...data,
    createdAt: moment(data.createdAt).format('YYYY-MM-DD hh:mm:ss'),
    expiresAt: moment(data.expiresAt).format('YYYY-MM-DD hh:mm:ss')
  };
};

export default new Vuex.Store({
  state: {
    encryptedHashes: [],
    decryptedHash: {},
    error: null,
  },
  mutations: {
    createSecretSuccess (state, data) {
      state.encryptedHashes.push(getDataWithDate(data));
      state.error = null;
    },
    getSecretSuccess (state, data) {
      state.decryptedHash = getDataWithDate(data);
      state.encryptedHashes = state.encryptedHashes.map(item => item.hash === data.hash ? getDataWithDate(data) : item);
      state.error = null;
    },
    setError (state, { error, hashToRemove }) {
      state.error = error;
      state.decryptedHash = {};

      if (hashToRemove) state.encryptedHashes = state.encryptedHashes.filter(item => item.hash !== hashToRemove);
    }
  },
  actions: {
    async createSecret ({ commit }, data) {
      const response = await request({
        url: '/secret',
          method: 'post',
          data
      });

      if (response.error) return commit('setError', response.error);

      commit('createSecretSuccess', response.data);
    },
    async getSecret ({ commit }, hash) {
      const response = await request({ url: `/secret/${hash}`, method: 'get' });

      if (response.error) return commit('setError', { error: response.error, hashToRemove: hash });

      commit('getSecretSuccess', response.data);
    }
  }
});
