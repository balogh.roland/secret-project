export default async ({ url, method, data }) => {
  try {
    const baseUrl = process.env.VUE_APP_SERVER_BASE_URL || 'http://localhost:3000/api';
    const fullURL = `${baseUrl}${url}`;

    const response = await fetch(fullURL, {
      method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    });

    return response.json();
  } catch (error) {
    console.log('Req error: ', error);

    return error.response || { ok: false, error };
  }
}