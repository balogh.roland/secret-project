# Secret server and client

## Development

### Server

- start a mongodb with docker: `docker run -p 27017:27017 -d mongo`
- go to `secret-server` directory
- create `.env` file based on `.env.example`
- run `npm install`
- run `npm run dev`

### Client

- go to `secret-client` directory
- create `.env` file based on `.env.example`
- run `npm install`
- run `npm run serve`

## Build
- create `.env` file based on `.env.example` in the projects
- go to your main directory, where the docker-compose file is
- run `docker-compose up`
